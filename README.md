# workman app



## Introduzione

  Workman è un applicativo web che permette di gestire le ore di permesso e i giorni di ferie personali
  non e pensato per essere un applicativo di gestione delle risorse umane ma solo un semplice strumento per tenere traccia delle ore di permesso e dei giorni di ferie personali.

## Funzionalità
  
  Workman permetterà all'utente di creare un suo profilo personale 
  
  L'utente potrà inserire le ore di permesso e i giorni di ferie personali e visualizzare il totale delle ore di permesso
  e dei giorni di ferie personali.
  
## Stato

  Workman é un progetto open source sviluppato in angular utilizzando il framework ionic per la distribuzione sui
  vari dispositivi mobili.
  
  Attualmente il progetto é in fase di sviluppo e non é ancora disponibile una versione stabile.
  
  Qualsiasi tipo di contributo é ben accetto. 
  
  É possibile usare il progetto in modo libero e gratuito. Per usi commerciali o per la distribuzione su app store
  si prega di contattare l'autore del progetto all'indirizzo alexandro4317@gmail.com
  
## Installazione
 
  Per installare il progetto è necessario avere installato node.js e npm.

  Bisognerà in seguito installare ionic e angular e configurare correttamente xcode o android studio per la 
  distribuzione dell'applicativo sui vari dispositivi mobili.
    
    Clonare il progetto da github
    eseguire il comando npm install
    eseguire il comando ionic capacitor build ios o ionic capacitor build android
    continuare sulle rispettive piattaforme con xcode o android studio per il deploy dell'app

